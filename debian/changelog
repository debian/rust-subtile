rust-subtile (0.3.2-2) unstable; urgency=medium

  * drop patch 2001_thiserror,
    obsoleted by Debian packaging changes;
    tighten (build-)dependency for crate thiserror
  * add metadata about upstream project
  * update git-buildpackage config:
    + filter out debian subdir
    + improve usage comment
  * update watch file:
    + improve filename and upstream version mangling
    + use Github API
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 08 Feb 2025 00:50:15 +0100

rust-subtile (0.3.2-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * relax (build-)dependencies for crate thiserror
  * bump project versions in virtual packages and autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 13 Jan 2025 00:56:59 +0100

rust-subtile (0.3.1-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * update and unfuzz patches
  * bump project versions in virtual packages and autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 10 Jan 2025 11:17:58 +0100

rust-subtile (0.3.0-2) unstable; urgency=medium

  * drop patch 2001_env_logger, obsoleted by Debian package changes;
    tighten build- and autopkgtest-dependencies
    for crate rust-env-logger

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 01 Jan 2025 13:41:56 +0100

rust-subtile (0.3.0-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * autopkgtest-depend on dh-rust (not dh-cargo)
  * stop mention dh-cargo in long description
  * bump project versions in virtual packages and autopkgtests
  * (build-)depend on package for crate iter_fixed
  * declare build-dependencies unconditionally,
    i.e. drop broken nocheck annotations
  * update copyright info: fix Upstream-Name
  * drop patch 2001_image, obsoleted by Debian packaging updates;
    tighten (build-)dependencies for crate image

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 16 Dec 2024 15:12:06 +0100

rust-subtile (0.1.9-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * drop patch cherry-picked upstream,
    obsoleted by upstream changes
  * unfuzz patches
  * stop (build-)depend on package for crate safemem
  * bump project versions in virtual packages and autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 12 Jul 2024 19:27:07 +0200

rust-subtile (0.1.8-5) unstable; urgency=medium

  * simplify packaging;
    build-depend on dh-sequence-rust
    (not dh-cargo libstring-shellquote-perl)

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 12 Jul 2024 18:21:36 +0200

rust-subtile (0.1.8-4) unstable; urgency=medium

  * no-changes source-only upload to enable testing migration

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 09 Jul 2024 19:38:19 +0200

rust-subtile (0.1.8-3) experimental; urgency=medium

  * add patches cherry-picked upstream
    to avoid convertion problem on i386;
    stop avoid checking test

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 09 Jul 2024 19:07:38 +0200

rust-subtile (0.1.8-2) unstable; urgency=medium

  * update dh-cargo fork
  * update copyright info: update coverage
  * skip test panicking on i386

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 05 Jul 2024 10:17:07 +0200

rust-subtile (0.1.8-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * bump project versions in virtual packages and autopkgtests
  * unfuzz patch 2001_image
  * fix patch 2001_profiling;
    closes: bug#1072657, thanks to Jeremy Bícha

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 07 Jun 2024 16:42:22 +0200

rust-subtile (0.1.7-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * update copyright info: fix file path
  * bump project versions in virtual packages and autopkgtests
  * drop patch cherry-picked upstream,
    obsoleted by upstream changes
  * unfuzz patches

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 25 May 2024 08:30:01 +0200

rust-subtile (0.1.4-2) unstable; urgency=medium

  * add patch cherry-picked upstream
    to change num primitive type to usize in nom call

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 12 Apr 2024 22:26:55 +0200

rust-subtile (0.1.4-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * unfuzz patches
  * add patch 2001_image to accept older branch of crate image;
    relax (build-)dependency for crate image
  * bump project versions in virtual packages and autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 12 Apr 2024 13:22:25 +0200

rust-subtile (0.1.3-2) unstable; urgency=medium

  * update copyright:
    + fix Upstream-Name
    + update coverage
  * declare compliance with Debian Policy 4.7.0
  * add DEP-3 patch headers for patch 1001

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 10 Apr 2024 06:47:30 +0200

rust-subtile (0.1.3-1) experimental; urgency=medium

  * initial packaging release;
    closes: bug#1014097

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 23 Mar 2024 21:26:17 +0100
